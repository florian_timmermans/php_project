C'est un site qui permet au prof administrateur d'ajouter une liste de professeur dans la base de données via des fichiers .csv.
Les profs qui sont dans la base de données peuvent à leur tour ajouter des élèves ainsi que séparer les classes en séries et gérer les cours.
(Le dossier Excel contient les fichiers utilisés pour les tests)